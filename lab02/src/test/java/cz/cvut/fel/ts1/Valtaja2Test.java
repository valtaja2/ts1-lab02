package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Valtaja2Test {

    Valtaja2 testClass = new Valtaja2();

    @Test
    void factorialTest() {
        assertEquals(1, testClass.factorial(1));
        assertEquals(6, testClass.factorial(3));
        assertNotEquals(6, testClass.factorial(0));

    }
}